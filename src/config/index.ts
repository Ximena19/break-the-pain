import useStore from "./redux";

const useConfig = () => {
    return {
        useStore,
    };
}

export default useConfig;
