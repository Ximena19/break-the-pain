import thunk from "redux-thunk";
import useModels from "../../models";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { persistReducer, persistStore } from "redux-persist";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from 'remote-redux-devtools';

/** Interfaces */
import { IPersistConfig } from "../../models/interfaces/config/Redux";

const useStore = () => {
    /** Reducers */
    const { useReducers } = useModels();
    
    /** Constants */
    let middleware : any[] = [thunk];
    const initialState : any = {},
          reducers = useReducers(),
          persistConfig : IPersistConfig = {
            key: "root",
            storage: AsyncStorage
          };

    const persistReduce = persistReducer(persistConfig, reducers);

    if(process.env.NODE_ENV === "development"){
        const reduxImmutable = require("redux-immutable-state-invariant").default();
        middleware = [...middleware, reduxImmutable];
    }

    const store = createStore(
        persistReduce,
        initialState,
        composeWithDevTools(applyMiddleware(...middleware))
    );

    const persister = persistStore(store);

    return {
        store,
        persister
    };
}

export default useStore;
