import React, {Suspense} from 'react';
import {Text} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import useConfig from './config';

const App = () => {
  const {useStore} = useConfig();
  const {persister, store} = useStore();

  console.log('Hi!!')

  return (
    <Provider store={store}>
      <PersistGate persistor={persister} loading={null}>
        <Suspense fallback={<Text>...Loading</Text>}>
          <Text>Hi!!</Text>
        </Suspense>
      </PersistGate>
    </Provider>
  );
};

export default App;
