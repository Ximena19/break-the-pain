import { useDispatch } from "react-redux";
import { Dispatch } from "redux";

const useActions = () => {
    /** Dispatch */
    const dispatch : Dispatch = useDispatch();

    return {
        dispatch,
    };
}

export default useActions;
