import axios from "axios";

const useProviders = () => {
    /** Axios Configuration */
    axios.defaults.baseURL = process.env.REACT_APP_API_URL;

    return {};
}

export default useProviders;
