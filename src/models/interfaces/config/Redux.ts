import AsyncStorage from "@react-native-async-storage/async-storage";

export interface IPersistConfig {
    key: string;
    storage: typeof AsyncStorage
}