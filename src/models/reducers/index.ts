import { combineReducers } from "redux";

const useReducers = () => {
    return combineReducers({});
}

export default useReducers;